package controllers;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Log controller.  Writes log information from objects as requested.
 * @author Anthony
 *
 */
public class LogController {
	private FileWriter fileWriter;
	private BufferedWriter outWriter;

	public LogController(String logFileName) {
		try {
			this.fileWriter = new FileWriter(logFileName);
			this.outWriter = new BufferedWriter(fileWriter);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void log(String stringToLog) {
		try {
			this.outWriter.write(stringToLog);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void logWithNewline(String stringToLog) {
		try {
			this.outWriter.write(stringToLog + "\n");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void shutdown() {
		try {
			this.outWriter.close();
			this.fileWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void logLineSeparator() {
		this.logWithNewline("--------------------------------------------------------------------------------");
	}

}
