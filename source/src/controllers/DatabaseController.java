package controllers;

import gis.GISRecord;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

import Indices.coordinate.client.CoordinateIndex;
import Indices.name.NameIndex;

/**
 * Database controller object responsible for writing to and fetching from the database file
 * @author Anthony
 *
 */
public class DatabaseController {
	private String dbFileName;
	private LogController logController;
	private RandomAccessFile dbFile;

	public DatabaseController(LogController logController, String dbFileName) {
		this.logController = logController;
		this.dbFileName = dbFileName;
		try {
			this.dbFile = new RandomAccessFile(dbFileName, "rw");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	public void importAndIndexRecords(String gisFileName,
			CoordinateIndex coordIndex, NameIndex nameIndex) {
		try {
			RandomAccessFile gisFile = new RandomAccessFile(gisFileName, "r");
			gisFile.seek(0);// go to beginning of file
			String line = gisFile.readLine(); // grab header line
			insertIntoDB(line); // log the header into the db
			long filePointer = gisFile.getFilePointer();
			if (line != null) {
				line = gisFile.readLine(); // skip header line by reading next line
			}
			// Log the offset and FID field of each GIS record
			int longestProbeSequence, importedLocations, importedFeaturesByName, probeSequenceLength;
			longestProbeSequence = importedLocations = importedFeaturesByName = probeSequenceLength = 0;
			while (line != null) {
				GISRecord record = new GISRecord(line);

				boolean insideWorld = coordIndex.inBounds(record);

				if (insideWorld) {
					long recordOffsetInDB = insertIntoDB(line);
					coordIndex.index(record, recordOffsetInDB);
					importedLocations++;
					probeSequenceLength = nameIndex.index(record,
							recordOffsetInDB);
					if (probeSequenceLength != -1) {
						importedFeaturesByName++;
					}

					if (probeSequenceLength > longestProbeSequence) {
						longestProbeSequence = probeSequenceLength;
					}
					filePointer = gisFile.getFilePointer();
					line = gisFile.readLine();
				}
			}
			logResults(importedFeaturesByName, longestProbeSequence,
					importedLocations);
			gisFile.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private long insertIntoDB(String line) {
		try {
			long recordOffset = this.dbFile.getFilePointer();
			this.dbFile.writeBytes(line + "\n");
			return recordOffset;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return -1;
	}

	private void logResults(int importedFeaturesByName,
			int longestProbeSequence, int importedLocations) {
		this.logController.logWithNewline("Imported Features by name:\t"
				+ importedFeaturesByName + "\n" + "Longest probe sequence:\t"
				+ longestProbeSequence + "\n" + "Imported Locations:\t"
				+ importedLocations);
	}

	public String fetch(long offset) {
		String line = null;
		try {
			this.dbFile.seek(offset);
			line = this.dbFile.readLine(); // grab header line
		} catch (IOException e) {
			e.printStackTrace();
		}
		return line;
	}
}
