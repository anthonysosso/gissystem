package controllers;

import gis.GISRecord;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

import Indices.bufferpool.BufferPool;
import Indices.coordinate.client.CoordinateIndex;
import Indices.name.NameIndex;

/**
 * Overall controller for the system. I chose to combine this with a commandprocessor object instead of having them
 * separated, as there was no clear way to encapsulte things sufficiently.
 * 
 * 
 * 
 * From the project spec: There should be an overall controller that validates the command line arguments and manages
 * the initialization of the various system components. The controller should hand off execution to a command processor
 * that manages retrieving commands from the script file, and making the necessary calls to other components in order to
 * carry out those commands.
 * 
 * @author Anthony
 * 
 */
public class Controller {

	private NameIndex nameIndex;
	private CoordinateIndex coordIndex;

	private Indices.bufferpool.BufferPool bufferPool;

	private LogController logController;
	private DatabaseController dbController;

	private int commandCounter;
	private String dbFileName;
	private String commandScriptFileName;
	private String logFileName;

	public Controller(String dbFileName, String commandScriptFileName, String logFileName) {
		this.dbFileName = dbFileName;

		this.commandScriptFileName = commandScriptFileName;
		this.logFileName = logFileName;
		this.logController = new LogController(logFileName);
		this.dbController = new DatabaseController(logController, dbFileName);
		this.bufferPool = new BufferPool(logController, dbController);
		this.nameIndex = new NameIndex(logController, bufferPool);

		this.coordIndex = new CoordinateIndex(logController, bufferPool);
	}

	public void start() {
		// TODO if the command script file is not found the program should write an error message to the console and
		// exit.
		String initString = "Author:  Anthony Sosso\n" + "Project:  Virginia Tech CS 3114 Major Project\n"
				+ "Log file: " + this.logFileName + "\n" + "Database file: " + this.dbFileName + "\n"
				+ "Commands file: " + this.commandScriptFileName + "\n"
				+ "Quadtree children are printed in the order SW  SE  NE  NW\n"
				+ "Latitude/longitude values in index entries are shown as signed integers, in total seconds.\n";
		this.logController.log(initString);
		this.logController.logLineSeparator();
		this.processCommands();
	}

	public void shutdown() {
		logController.shutdown();
	}

	/**
	 * Reads a commands file, calls the appropriate function to process the command, and logs the command to the output
	 * log file
	 * 
	 * @param logFileName
	 *            name of the log file to write output to Calls GISParser.report for report commands
	 */
	public void processCommands() {
		File commandsFile = new File(this.commandScriptFileName);
		try {
			Scanner commandsScanner = new Scanner(commandsFile);
			commandsScanner.useDelimiter("\n");
			this.logController.logWithNewline("");
			commandCounter = 1;
			while (commandsScanner.hasNext()) {
				String line = commandsScanner.next();
				if (line.startsWith(";")) { // log comment lines
					logController.logWithNewline(line);
				} else {
					String elements[] = line.split("\t");
					String command = elements[0];
					
					//Since we can't use java 7, have to switch on an integer or enum; I'm using integers.
					ArrayList<String> commandNames = new ArrayList<String>(Arrays.asList( "world", "import", "what_is_at", "what_is", "what_is_in", "debug", "quit" ));
					int commandIndex = commandNames.indexOf(command);
					switch (commandIndex) {
					case 0:
						processWorld(elements);
						commandCounter--;// world doesn't count, and we increment the counter after the switch, so -1
											// here.
						break;
					case 1:
						processImport(elements);
						break;
					case 2:
						processWhatIsAt(elements);
						break;
					case 3:
						processWhatIs(elements);
						break;
					case 4:
						processWhatIsIn(elements);
						break;
					case 5:
						processDebug(elements);
						break;
					case 6:
						processQuit(elements);
						break;
					default:
						commandCounter--;
						break;
					}
					commandCounter++;
					logController.logLineSeparator();
				}
			}
			commandsScanner.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

	}

	private void processWorld(String[] args) {
		logCommand(args);
		this.logController.logWithNewline("North boundary: " + GISRecord.getSeconds(args[4]));
		this.logController.logWithNewline("South boundary: " + GISRecord.getSeconds(args[3]));
		this.logController.logWithNewline("East boundary: " + GISRecord.getSeconds(args[2]));
		this.logController.logWithNewline("West boundary: " + GISRecord.getSeconds(args[1]));
		this.coordIndex.setBoundaries(args);
	}

	private void processImport(String[] args) {
		logCommand(args);
		this.dbController.importAndIndexRecords(args[1], this.coordIndex, this.nameIndex);
	}

	private void processWhatIsAt(String[] args) {
		logCommand(args);
		String switchString = null;
		if (args.length == 4) {// no switch
			switchString = args[1];
		}
		this.coordIndex.what_is_at(switchString, args[args.length - 2], args[args.length - 1]);
	}

	private void processWhatIs(String[] args) {
		logCommand(args);
		String switchString = null;
		if (args.length == 4) {// no switch
			switchString = args[1];
		}
		this.nameIndex.whatIs(switchString, args[args.length - 2], args[args.length - 1]);
	}

	private void processWhatIsIn(String[] args) {
		logCommand(args);
		String switchString = null;
		if (args.length == 6) {// no switch
			switchString = args[1];
		}
		this.coordIndex.what_is_in(switchString, args[args.length - 4], args[args.length - 3],
				Long.parseLong(args[args.length - 2]), Long.parseLong(args[args.length - 1]));
	}

	private void processQuit(String[] args) {
		logCommand(args);
	}

	private void processDebug(String[] args) {
		logCommand(args);
		String structure = args[1];
		ArrayList<String> structureNames = new ArrayList<String>(Arrays.asList("quad","hash","pool"));
		int structureIndex = structureNames.indexOf(structure);
		switch (structureIndex) {
		case 0:
			this.logController.logWithNewline(this.coordIndex.toString());
			break;
		case 1:
			this.logController.logWithNewline(this.nameIndex.toString());
			break;
		case 2:
			this.logController.logWithNewline(this.bufferPool.toString());
			break;
		}
	}

	private void logCommand(String[] commandLine) {
		StringBuilder builder = new StringBuilder();
		if (!commandLine[0].equals("world")) {
			builder.append("Command " + this.commandCounter + " : ");
		}
		builder.append(commandLine[0]);

		for (int i = 1; i < commandLine.length; i++) {// append all args except args[0], which is the command name
			builder.append("\t" + commandLine[i]);
		}
		this.logController.logWithNewline(builder.toString());
	}

}
