package Indices.bufferpool;

import gis.GISRecord;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import controllers.DatabaseController;
import controllers.LogController;

/**
 * Buffer pool object to avoid fetching from disk if possible.
 * 
 * From the spec: The system will include a buffer pool, as a front end for the GIS database file, to improve search
 * speed. See the discussion of the buffer pool below for detailed requirements. When performing searches, retrieving a
 * GIS record from the database file must be managed through the buffer pool. During an import operation, when records
 * are written to the database file, the buffer pool will be bypassed, since the buffer pool would not improve
 * performance during imports.
 * 
 * The buffer pool for the database file should be capable of buffering up to 20 records, and will use LRU replacement.
 * You may use any structure you like to organize the pool slots; however, since the pool will have to deal with record
 * replacements, some structures will be more efficient (and simpler) to use. It is up to you to decide whether your
 * buffer pool stores interpreted or raw data; i.e., whether the buffer pool stores GIS record objects or just strings.
 * 
 * You must be able to display the contents of the buffer pool, listed from MRU to LRU entry, in a readable manner. The
 * order in which you retrieve records when servicing a multi-match search is not specified, so such searches may result
 * in different orderings of the records within the buffer pool. That is OK.
 * 
 * @author Anthony
 * 
 */
// Front of the list is the MOST RECENTLY USED
public class BufferPool {
	private static final int MAX_SIZE = 20; // max size of the buffer pool

	private LogController logController;
	private LinkedList<RecordAndOffset> records;
	private DatabaseController dbController;

	public BufferPool(LogController logController, DatabaseController dbController) {
		this.logController = logController;
		this.records = new LinkedList<RecordAndOffset>();
		this.dbController = dbController;
	}

	public GISRecord get(long offset) {
		RecordAndOffset recordToPromoteAsRecentlyUsed = null;
		for (RecordAndOffset recordAndOffset : this.records) {
			if (recordAndOffset.getOffset() == offset) {
				recordToPromoteAsRecentlyUsed = recordAndOffset;
				break;
			}
		}
		if (recordToPromoteAsRecentlyUsed == null) {// none found, go fetch
			String gisRecordString = this.dbController.fetch(offset);
			GISRecord record = new GISRecord(gisRecordString);
			RecordAndOffset ro = new RecordAndOffset(record, offset);
			addRecord(ro);
			return record;
		} else {// found, promote to MRU
			this.records.remove(recordToPromoteAsRecentlyUsed);// move record to the MRU spot
			addRecord(recordToPromoteAsRecentlyUsed);
			return recordToPromoteAsRecentlyUsed.getRecord();
		}
	}

	public List<GISRecord> getAll(List<Long> offsets) {
		List<GISRecord> records = new ArrayList<GISRecord>();
		for (long offset : offsets) {
			GISRecord record = this.get(offset);
			if (record != null) {
				records.add(record);
			}
		}
		return records;
	}

	private void addRecord(RecordAndOffset ro) {
		if (this.records.size() == MAX_SIZE) {// need to evict the LRU element
			this.records.removeLast();
		}
		this.records.add(0, ro);
	}

	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("MRU\n");
		for (RecordAndOffset ro : this.records) {
			builder.append(ro.toString() + "\n");
		}
		builder.append("LRU");
		return builder.toString();
	}

}
