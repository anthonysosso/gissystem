package Indices.bufferpool;

import gis.GISRecord;

/**
 * Class of objects stored by the buffer pool.  Thin wrapper on GISRecords to store their location in the database file.
 * @author Anthony
 *
 */
public class RecordAndOffset {
	private GISRecord record;
	private long offset;

	public RecordAndOffset(GISRecord record, long offset) {
		super();
		this.record = record;
		this.offset = offset;
	}

	public GISRecord getRecord() {
		return record;
	}

	public long getOffset() {
		return offset;
	}
	
	public String toString(){
		return offset + ":\t" + record.gisFileRepresentation();
	}

}
