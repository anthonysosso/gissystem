package Indices.coordinate.ds;

/**
 * Exception to throw whenever a leaf is full. Should never occur if partitioning/conversion to internal nodes is done
 * properly.
 * 
 * @author Anthony
 * 
 */
public class LeafFullException extends Exception {

}
