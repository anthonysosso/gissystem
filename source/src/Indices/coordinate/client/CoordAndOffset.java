package Indices.coordinate.client;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import Indices.coordinate.ds.Compare2D;
import Indices.coordinate.ds.Direction;

/**
 * Class of object to be stored by the quadtree.  Stores the coordinates and offset(s) of records in the tree.
 * @author Anthony
 *
 */
public class CoordAndOffset implements Compare2D<CoordAndOffset> {

	private long xcoord;
	private long ycoord;
	private List<Long> fileOffsets;

	public CoordAndOffset() {
		this.xcoord = 0;
		this.ycoord = 0;
		this.fileOffsets = new ArrayList<Long>();
	}

	public CoordAndOffset(long x, long y, long offset) {
		this.xcoord = x;
		this.ycoord = y;
		this.fileOffsets = new ArrayList<Long>(Arrays.asList(offset));
	}

	public long getX() {
		return xcoord;
	}

	public long getY() {
		return ycoord;
	}

	public List<Long> getFileOffsets() {
		return fileOffsets;
	}

	public Direction directionFrom(long X, long Y) {
		long xDiff = this.xcoord - X;
		long yDiff = this.ycoord - Y;
		if (X == this.xcoord && Y == this.ycoord) {
			return Direction.NOQUADRANT;
		} else if (xDiff >= 0) {
			if (yDiff >= 0) {
				return Direction.NE;
			} else {
				return Direction.SE;
			}
		} else { // xdiff < 0
			if (yDiff >= 0) {
				return Direction.NW;
			} else {
				return Direction.SW;
			}
		}
	}

	public Direction inQuadrant(double xLo, double xHi, double yLo, double yHi) {
		double yAxisLoc = (xLo + xHi) / 2.0;
		double xAxisLoc = (yLo + yHi) / 2.0;
		double x = (double) this.xcoord;
		double y = (double) this.ycoord;
		if (!inBox(xLo, xHi, yLo, yHi)) {
			return Direction.NOQUADRANT;
		} else if (x > yAxisLoc && y >= xAxisLoc) {
			return Direction.NE;
		} else if (x <= yAxisLoc && y > xAxisLoc) {
			return Direction.NW;
		} else if (x < yAxisLoc && y <= xAxisLoc) {
			return Direction.SW;
		} else if (x >= yAxisLoc && y < xAxisLoc) {
			return Direction.SE;
		}

		return Direction.NOQUADRANT;
	}

	// TODO shouldn't need to swap these numbers.
	public boolean inBox(double xLo, double xHi, double yLo, double yHi) {
		/*
		 * double yLo2 = yLo; double yHi2 = yHi; double xLo2 = xLo; double xHi2 = xHi; if (yLo > yHi) { // negative
		 * numbers probably yLo2 = yHi; yHi2 = yLo; } if (xLo > xHi) { xLo2 = xHi; xHi2 = xLo; }
		 */
		return (this.getX() >= xLo && this.getX() <= xHi)
				&& (this.getY() >= yLo && this.getY() <= yHi);
	}

	public void addOffset(long offset) {
		this.fileOffsets.add(offset);
	}

	public String toString() {
		return "(" + this.getX() + ", " + this.getY() + ";"
				+ this.getFileOffsetsAsString() + ")";
	}

	public String getFileOffsetsAsString() {
		StringBuilder b = new StringBuilder();
		for (long fileOffset : this.fileOffsets) {
			b.append(fileOffset + ";");
		}
		b.deleteCharAt(b.lastIndexOf(";")); // remove the trailing semicolon
		return b.toString();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CoordAndOffset other = (CoordAndOffset) obj;
		if (xcoord != other.xcoord)
			return false;
		if (ycoord != other.ycoord)
			return false;
		return true;
	}

}
