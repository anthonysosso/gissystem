package Indices.coordinate.client;

import static org.junit.Assert.assertEquals;
import gis.GISRecord;

import java.util.List;
import java.util.Vector;

import org.junit.Test;

import Indices.bufferpool.BufferPool;
import controllers.LogController;

/**
 * unit tests for coordinate index
 * @author Anthony
 *
 */
public class CoordinateIndexTest {

	/**
	 * Find a coordinate with 1 offset; Find a record not in the table (negative test); Find a coordinate with 2 offsets
	 */
	@Test
	public void testFindByCoordinate() {
		LogController nameIndexLogger = new LogController("footest");
		CoordinateIndex cIndex = new CoordinateIndex(nameIndexLogger, new BufferPool(nameIndexLogger, null));
		cIndex.setBoundaries(-10, 10, -10, 10);
		GISRecord record = new GISRecord("Place1", "000000" + 5 + "N", "000000" + 5 + "W");
		cIndex.index(record, 1);
		record = new GISRecord("Place2", "000000" + 5 + "N", "000000" + 5 + "E");
		cIndex.index(record, 2);
		cIndex.index(record, 20);
		record = new GISRecord("Place3", "000000" + 5 + "S", "000000" + 5 + "W");
		cIndex.index(record, 3);
		record = new GISRecord("Place4", "000000" + 5 + "S", "000000" + 5 + "E");
		cIndex.index(record, 4);
		record = new GISRecord("Place5", "000000" + 7 + "N", "000000" + 7 + "W");
		cIndex.index(record, 5);

		// Find the single offset at 5N 5W
		List<Long> offsets = cIndex.find(5, -5);
		assertEquals(offsets.size(), 1);

		// Find no record at 6N 5E
		offsets = cIndex.find(6, 5);
		assertEquals(offsets.size(), 0);
		// Find no record at 5N 6E
		offsets = cIndex.find(5, 6);
		assertEquals(offsets.size(), 0);

		// Find the two offsets located at 5N 5E
		offsets = cIndex.find(5, 5);
		assertEquals(offsets.size(), 2);
		nameIndexLogger.shutdown();
	}

	/**
	 * Find one element in a region Find no elements in an empty region Find all elements in the table
	 */
	@Test
	public void testFindByRegion() {
		LogController nameIndexLogger = new LogController("footest");
		CoordinateIndex cIndex = new CoordinateIndex(nameIndexLogger, null);
		cIndex.setBoundaries(-10, 10, -10, 10);

		// A northwest record
		GISRecord record = new GISRecord("Place1", "000000" + 5 + "N", "000000" + 5 + "W");
		cIndex.index(record, 1);

		// Two northeast records, one with two offsets
		record = new GISRecord("Place2", "000000" + 5 + "N", "000000" + 5 + "E");
		cIndex.index(record, 2);
		cIndex.index(record, 20);
		record = new GISRecord("Place2", "000000" + 6 + "N", "000000" + 6 + "E");
		cIndex.index(record, 6);

		// Two southwest records
		record = new GISRecord("Place3", "000000" + 5 + "S", "000000" + 5 + "W");
		cIndex.index(record, 3);
		record = new GISRecord("Place5", "000000" + 7 + "S", "000000" + 7 + "W");
		cIndex.index(record, 5);

		Vector<CoordAndOffset> offsets;

		// Find the single offset at 5N 5W
		offsets = cIndex.find(-10, 0, 0, 10);// NW quadrant
		assertEquals(1, offsets.size());
		// Make sure there's only one offset
		assertEquals(1, getOffsetCount(offsets));

		// Find no record in SE quadrant
		offsets = cIndex.find(0, 10, -10, 0);
		assertEquals(0, offsets.size());
		// Shouldn't be any offsets either
		assertEquals(0, getOffsetCount(offsets));

		// Find the two records located in the NE quadrant
		offsets = cIndex.find(0, 10, 0, 10);
		assertEquals(2, offsets.size());
		// There should be 3 offsets
		assertEquals(3, getOffsetCount(offsets));

		// Find all elements in the table
		offsets = cIndex.find(-10, 10, -10, 10);
		assertEquals(5, offsets.size());
		// Should be 6 offsets
		assertEquals(6, getOffsetCount(offsets));

		nameIndexLogger.shutdown();
	}

	private int getOffsetCount(Vector<CoordAndOffset> offsets) {
		int offsetCount = 0;
		for (CoordAndOffset offset : offsets) {
			offsetCount += offset.getFileOffsets().size();
		}
		return offsetCount;
	}

	@Test
	public void testToString() {
		LogController nameIndexLogger = new LogController("name_index_test.txt");
		CoordinateIndex cIndex = new CoordinateIndex(nameIndexLogger, null);
		cIndex.setBoundaries(-10, 10, -10, 10);

		// A northwest record
		GISRecord record = new GISRecord("Place1", "000000" + 5 + "N", "000000" + 5 + "W");
		cIndex.index(record, 1);

		// Two northeast records, one with two offsets
		record = new GISRecord("Place2", "000000" + 5 + "N", "000000" + 5 + "E");
		cIndex.index(record, 2);
		cIndex.index(record, 20);
		record = new GISRecord("Place2", "000000" + 6 + "N", "000000" + 6 + "E");
		cIndex.index(record, 6);

		// Two southwest records
		record = new GISRecord("Place3", "000000" + 5 + "S", "000000" + 5 + "W");
		cIndex.index(record, 3);
		record = new GISRecord("Place5", "000000" + 7 + "S", "000000" + 7 + "W");
		cIndex.index(record, 5);
		nameIndexLogger.logWithNewline(cIndex.toString());
		nameIndexLogger.shutdown();
	}

}
