package Indices.name;


/**
 * When using integers, quadratic probing resulted in overflow and negative slot calculations.  This exception is used to trap that case.
 * @author Anthony
 *
 */
public class NegativeHomeSlotException extends Exception {

}
