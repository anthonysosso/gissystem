package Indices.name;

import static org.junit.Assert.assertEquals;
import gis.GISRecord;

import java.util.List;

import org.junit.Test;

import controllers.LogController;

/**
 * Unit tests for name index
 * @author Anthony
 *
 */
public class NameIndexTest {

	/**
	 * Find a lone record;
	 * Find a record not in the table (negative test);
	 * Find a duplicate record
	 */
	@Test
	public void testFind() {
		LogController nameIndexLogger = new LogController("name_index_test.txt");
		NameIndex nIndex = new NameIndex(nameIndexLogger, null);
		for (int i = 0; i < 10; i++) {
			GISRecord record = new GISRecord("fooName" + i, "fooAbbrev" + i);
			nIndex.index(record, (long) i);
		}

		// Find a lone record
		List<Long> offsets = nIndex.find("fooName2", "fooAbbrev2");
		assertEquals(1, offsets.size());

		// find a nonexistent record
		assertEquals(0, nIndex.find("fooName-2", "fooAbbrev-2").size());

		// find a duplicate record
		GISRecord record = new GISRecord("fooName1", "fooAbbrev1");
		nIndex.index(record, -1);
		offsets = nIndex.find("fooName1", "fooAbbrev1");
		assertEquals(2, offsets.size()); // will fail because it finds it twice due to probing bug

		nameIndexLogger.shutdown();
	}

	/**
	 * Make sure the hash map resizes at 70% capacity.
	 */
	@Test
	public void testResize() {
//		ArrayList<Integer> arraySizes = new ArrayList<Integer>(Arrays.asList(
//				1019, 2027, 4079, 8123, 16267, 32503, 65011, 130027, 260111,
//				520279, 1040387, 2080763, 4161539, 8323151, 16646323));
		LogController nameIndexLogger = new LogController("name_index_test.txt");
		NameIndex nIndex = new NameIndex(nameIndexLogger, null);
			int numToInsertWithoutResize = (int) (Math.ceil(0.7 * 1019));
			for (int j = 0; j < numToInsertWithoutResize; j++) {
				GISRecord record = new GISRecord("fooName" + j, "fooAbbrev" + j);
				nIndex.index(record, (long) j);
			}
			assertEquals(1019, nIndex.getHashSize());
			GISRecord recordToMakeItResize = new GISRecord("fooNameResizer", "fooAbbrevResizer");
			nIndex.index(recordToMakeItResize, (long) 0);
			assertEquals(2027, nIndex.getHashSize());
		}
}
