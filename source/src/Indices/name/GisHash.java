package Indices.name;

import gis.GISRecord;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Hash table class for GisHashEntry objects. Quadratic probing
 * 
 * @author Anthony
 * 
 */
public class GisHash {
	private static ArrayList<Integer> arraySizes = new ArrayList<Integer>(Arrays.asList(1019, 2027, 4079, 8123, 16267,
			32503, 65011, 130027, 260111, 520279, 1040387, 2080763, 4161539, 8323151, 16646323));

	private GisHashEntry[] array;
	private long insertedEntryCount;

	public GisHash(int arraySize) {
		this.insertedEntryCount = 0;
		this.array = new GisHashEntry[arraySize];
	}

	public GisHash() {
		this.insertedEntryCount = 0;
		this.array = new GisHashEntry[arraySizes.get(0)];
	}

	public int insert(GISRecord record, long fileOffset) {
		int probeSequenceLength = 0;
		resizeIfNecessary();

		GisHashEntry entry = new GisHashEntry(record.getFeatureName(), record.getStateAlphabeticCode(), fileOffset);
		probeSequenceLength = insertHelper(entry, this.array);
		return probeSequenceLength;
	}

	private int insertHelper(GisHashEntry entry, GisHashEntry[] destinationArray) {
		int homeSlot = entry.elfHash() % destinationArray.length;
		int originalHomeSlot = homeSlot; // sanity check to make sure we never
											// get back to the original
		int probeSequenceLength = 0;
		GisHashEntry homeSlotElement = destinationArray[homeSlot];
		while (homeSlotElement != null && !homeSlotElement.equals(entry)) { // probe until we find a key match
			probeSequenceLength++;
			homeSlot = computeNextSlotToCheck(originalHomeSlot, probeSequenceLength, destinationArray.length);
			if (homeSlot == originalHomeSlot) {
				try {
					throw new GisHashFullException();
				} catch (GisHashFullException e) {
					e.printStackTrace();
				}
			}
			homeSlotElement = destinationArray[homeSlot];
		}
		if (homeSlotElement == null) {// true insertion
			destinationArray[homeSlot] = entry;
			this.insertedEntryCount++;
		} else {// just appending an offset
			homeSlotElement.addFileOffset(entry.getFileOffsets().get(0));
		}

		return probeSequenceLength;

	}

	public List<Long> find(String featureName, String stateAlphabeticCode) {
		List<Long> matchingRecords = new ArrayList<Long>();
		GisHashEntry mockEntry = new GisHashEntry(featureName, stateAlphabeticCode, -1);
		int startingHomeSlot = mockEntry.elfHash() % this.array.length;
		if (this.array[startingHomeSlot] != null && this.array[startingHomeSlot].equals(mockEntry)) {
			matchingRecords.addAll(this.array[startingHomeSlot].getFileOffsets());
		} else {
			int iteration = 1;
			int currentSlotToCheck = computeNextSlotToCheck(startingHomeSlot, iteration);

			while (currentSlotToCheck != startingHomeSlot) { // search the whole table until we've wrapped to where we
																// started unless we find the key match
				if (this.array[currentSlotToCheck] != null && this.array[currentSlotToCheck].equals(mockEntry)) {
					matchingRecords.addAll(this.array[currentSlotToCheck].getFileOffsets());
					break;
				}
				iteration++;
				currentSlotToCheck = computeNextSlotToCheck(startingHomeSlot, iteration, this.array.length);
			}
		}
		return matchingRecords;
	}

	private int computeNextSlotToCheck(int startingSlot, int iteration, int arraySize) {// use quadratic probing
																						// equation (n*n + n)/2
		long nextSlot = ((long) startingSlot + (((long) iteration * (long) iteration + (long) iteration) / (long) 2))
				% (long) arraySize; // use long to avoid overflow with the n^2
		if (nextSlot < 0) {
			try {
				throw new NegativeHomeSlotException();
			} catch (NegativeHomeSlotException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return (int) nextSlot;
	}

	// convenient helper so I don't have to pass this.array.length unless I want to
	private int computeNextSlotToCheck(int startingSlot, int iteration) {
		return computeNextSlotToCheck(startingSlot, iteration, this.array.length);
	}

	private float computeLoadFactor() {
		if (this.array.length <= 0) {
			return 0;
		}
		return (float) this.insertedEntryCount / (float) this.array.length;
	}

	private void resizeIfNecessary() {
		if (computeLoadFactor() >= 0.7) {
			int nextTableSize = 0;
			if (arraySizes.indexOf(this.array.length) == -1) {// we've resized past the predefined set
				nextTableSize = this.array.length * 2 + 3;
			} else {
				nextTableSize = arraySizes.get(arraySizes.indexOf(this.array.length) + 1);
			}

			if (nextTableSize != 0) {
				this.insertedEntryCount = 0; // clear this so we avoid
												// overcounting
				GisHashEntry[] newArray = new GisHashEntry[nextTableSize];
				for (GisHashEntry entry : this.array) {
					if (entry != null) {
						insertHelper(entry, newArray);
					}
				}
				this.array = newArray;
			}
		}
	}

	public String toString() {
		StringBuilder builder = new StringBuilder();

		builder.append("Format of display is\n" + "Slot number: data record\n" + "Current table size is "
				+ this.array.length + "\n" + "Number of elements in table is " + this.insertedEntryCount + "\n\n");

		for (int i = 0; i < this.array.length; i++) {
			if (this.array[i] != null) {
				builder.append(i + ":\t" + this.array[i].toString() + "\n");
			}
		}

		return builder.toString();
	}

	public int getHashSize() {
		// TODO Auto-generated method stub
		return this.array.length;
	}

}
