package Indices.name;

/**
 * Exception to throw when the hash table is full.  Shouldn't happen if load factor-induced resizing is done properly.
 * @author Anthony
 *
 */
public class GisHashFullException extends Exception {

}
