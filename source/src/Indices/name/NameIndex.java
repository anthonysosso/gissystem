package Indices.name;

import gis.GISRecord;

import java.util.List;

import Indices.bufferpool.BufferPool;
import controllers.LogController;

/**
 * From the project spec: The name index will use a hash table for its physical organization. Each hash table entry
 * will store a feature name and state abbreviation (separately or concatenated, as you like) and the file offset(s) of
 * the matching record(s). Since each GIS record occupies one line in the file, it is a trivial matter to locate and
 * read a record given nothing but the file offset at which the record begins.
 * 
 * The hash table must use a contiguous physical structure (array). The initial size of the table will be 1019, and the
 * table will resize itself automatically. The precise logic for resizing is up to you; if you want to match my output
 * logs, increase the size of the table to the next value in the list below when the table becomes 70% full: 1019, 2027,
 * 4079, 8123, 16267, 32503, 65011, 130027, 260111, 520279, 1040387, 2080763, 4161539, 8323151, 16646323
 * 
 * Your table will use quadratic probing to resolve collisions, with the quadratic function (n2 + n)/2 to compute the
 * step size. Since the table sizes given above are all primes of the form 4k + 3, an empty slot will always be found
 * unless the table is full.
 * 
 * You will use the elfhash() function from the course notes, and apply it to the concatenation of the feature name and
 * state (abbreviation) field of the data records. Precisely how you form the concatenation is up to you.
 * 
 * You must be able to display the contents of the hash table in a readable manner.
 * 
 * @author Anthony
 * 
 */
public class NameIndex {
	private LogController logController;
	private GisHash hashmap;
	private BufferPool bufferPool;

	public NameIndex(LogController logController, BufferPool bufferPool) {
		this.logController = logController;
		this.hashmap = new GisHash();
		this.bufferPool = bufferPool;
	}

	// returns the length of the probe sequence
	public int index(GISRecord record, long fileOffset) {
		return this.hashmap.insert(record, fileOffset);
	}

	protected List<Long> find(String featureName, String stateAlphabeticCode) {
		return this.hashmap.find(featureName, stateAlphabeticCode);
	}

	public String toString() {
		return this.hashmap.toString();
	}


	public void whatIs(String switchString, String featureName, String stateAlphabeticCode) {
		List<Long> offsets = this.find(featureName, stateAlphabeticCode);
		if (offsets.size() == 0) {
			this.logController.logWithNewline("No records match " + featureName + " and " + stateAlphabeticCode);
		} else {
			List<GISRecord> recordsFromPool = this.bufferPool.getAll(offsets);
			if ("-c".equals(switchString)) {
				this.logController.logWithNewline("The number of records for " + featureName + " and "
						+ stateAlphabeticCode + " was " + offsets.size());
			} else {
				for (GISRecord record : recordsFromPool) {
					long offset = offsets.get(recordsFromPool.indexOf(record));
					if ("-l".equals(switchString)) {
						this.logController.logWithNewline("Found matching record at offset " + offset + ":");
						this.logController.logWithNewline(record.nonEmptyFieldToString());
					} else {
						this.logController.logWithNewline(offset + ":\t" + record.toStringTypeLatLong());
					}
					this.logController.logWithNewline("");
				}
			}
		}

	}

	/**
	 * only used for the unit tests to check the size of the table
	 * @return
	 */
	protected int getHashSize() {
		return this.hashmap.getHashSize();
	}
}
