package Indices.name;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Stores feature name + state abbreviation with offset(s) of corresponding records.  These are what the name index stores.
 * @author Anthony
 *
 */
public class GisHashEntry {

	private String featureName;
	private String stateAbbreviation;
	private List<Long> fileOffsets; 

	public GisHashEntry(String featureName, String stateAbbreviation,
			long fileOffset) {
		super();
		this.featureName = featureName;
		this.stateAbbreviation = stateAbbreviation;
		this.fileOffsets = new ArrayList<Long>(Arrays.asList(fileOffset));
	}

	public String getFeatureName() {
		return featureName;
	}

	public String getStateAbbreviation() {
		return stateAbbreviation;
	}

	public void addFileOffset(long fileOffset){
		this.fileOffsets.add(fileOffset);
	}
	
	public List<Long> getFileOffsets(){
		return this.fileOffsets;
	}
	
	public String getFileOffsetsAsString() {
		StringBuilder b = new StringBuilder();
		for(long fileOffset : this.fileOffsets){
			b.append(fileOffset + ";");
		}
		b.deleteCharAt(b.lastIndexOf(";")); // remove the trailing semicolon
		return b.toString();
	}

	public String toString() {
		return "[" + this.featureName + ":" + this.stateAbbreviation + ", ["
				+ this.getFileOffsetsAsString()+ "]]";
	}

	public int elfHash() {
		String toHash = this.featureName + this.stateAbbreviation;
		int hashValue = 0;
		for (int Pos = 0; Pos < toHash.length(); Pos++) { // use all elements
			hashValue = (hashValue << 4) + toHash.charAt(Pos); // shift/mix
			int hiBits = hashValue & 0xF0000000; // get high nybble
			if (hiBits != 0)
				hashValue ^= hiBits >> 24; // xor high nybble with second nybble
			hashValue &= ~hiBits; // clear high nybble
		}
		return hashValue;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GisHashEntry other = (GisHashEntry) obj;
		if (featureName == null) {
			if (other.featureName != null)
				return false;
		} else if (!featureName.equals(other.featureName))
			return false;
		if (stateAbbreviation == null) {
			if (other.stateAbbreviation != null)
				return false;
		} else if (!stateAbbreviation.equals(other.stateAbbreviation))
			return false;
		return true;
	}

}
