package gis;

/**
 * 
 Anthony R Sosso III
 * CS 3114 Project 1
 * 9/5/2012
 */

/**
 * Class modeling the fields of a GIS record with toString variants as required by the spec.
 * 
 * @author Anthony
 * 
 */
public class GISRecord {
	private int featureID; // unsigned
	private String featureName;
	private String featureClass;
	private String stateAlphabeticCode; // 2 chars
	private int stateNumericCode; // unsigned
	private String countyName;
	private int countyNumericCode; // unsigned
	private String primaryLatitudeDMS;
	private String primaryLongitudeDMS;
	private double primaryLatitudeDecDeg;
	private double primaryLongitudeDecDeg;
	private String sourceLatitudeDMS;
	private String sourceLongitudeDMS;
	private double sourceLatitudeDecDeg;
	private double sourceLongitudeDecDeg;
	private int featureElevationMeters;
	private int featureElevationFeet;
	private String mapName;
	private String dateCreated;
	private String dateEdited;

	/**
	 * @param delimitedGISData
	 *            Pipe-delimited GIS data string
	 */
	public GISRecord(String delimitedGISData) {
		super();
		String gisData[] = delimitedGISData.split("\\|");
		if (!gisData[0].equals("")) {
			this.featureID = Integer.parseInt(gisData[0]);
		} else {
			System.out.println("EmptyFID");
		}

		this.featureName = gisData[1];
		this.featureClass = gisData[2];
		this.stateAlphabeticCode = gisData[3];
		if (!gisData[4].equals("")) {
			this.stateNumericCode = Integer.parseInt(gisData[4]);
		}
		this.countyName = gisData[5];
		if (!gisData[6].equals("")) {
			this.countyNumericCode = Integer.parseInt(gisData[6]);
		}

		this.primaryLatitudeDMS = gisData[7];
		this.primaryLongitudeDMS = gisData[8];
		if (!gisData[9].equals("")) {
			this.primaryLatitudeDecDeg = Double.parseDouble(gisData[9]);
		}
		if (!gisData[10].equals("")) {
			this.primaryLongitudeDecDeg = Double.parseDouble(gisData[10]);
		}

		this.sourceLatitudeDMS = gisData[11];
		this.sourceLongitudeDMS = gisData[12];
		if (!gisData[13].equals("")) {
			this.sourceLatitudeDecDeg = Double.parseDouble(gisData[13]);
		}
		if (!gisData[14].equals("")) {
			this.sourceLongitudeDecDeg = Double.parseDouble(gisData[14]);
		}
		if (!gisData[15].equals("")) {
			this.featureElevationMeters = Integer.parseInt(gisData[15]);
		}
		if (!gisData[16].equals("")) {
			this.featureElevationFeet = Integer.parseInt(gisData[16]);
		}
		this.mapName = gisData[17];
		this.dateCreated = gisData[18];
		if (gisData.length == 20) {
			this.dateEdited = gisData[19];
		}

	}

	// useful for creating 'mock' records for name index testing
	public GISRecord(String featureName, String stateAlphabeticCode) {
		this.featureName = featureName;
		this.stateAlphabeticCode = stateAlphabeticCode;
	}

	// useful for creating 'mock' records for coordinate index testing
	public GISRecord(String featureName, String primaryLatitudeDMS, String primaryLongitudeDMS) {
		this.featureName = featureName;
		this.primaryLongitudeDMS = primaryLongitudeDMS;
		this.primaryLatitudeDMS = primaryLatitudeDMS;
	}

	public String toString() {
		return this.featureID + "\t" + this.featureName + "\t" + this.prettyPrimaryLatitudeDMS() + "\t"
				+ this.prettyPrimaryLongitudeDMS();
	}

	public String nameStateCoordinateToString() {
		StringBuilder builder = new StringBuilder();
		if (this.featureName != null && !this.featureName.equals("")) {
			builder.append(this.featureName + "\t");
		}
		if (this.stateAlphabeticCode != null && !this.stateAlphabeticCode.equals("")) {
			builder.append(this.stateAlphabeticCode + "\t");
		}
		if (this.primaryLatitudeDMS != null && !this.primaryLatitudeDMS.equals("")) {
			builder.append(this.primaryLatitudeDMS + "\t");
		}
		if (this.primaryLongitudeDMS != null && !this.primaryLongitudeDMS.equals("")) {
			builder.append(this.primaryLongitudeDMS);
		}
		return builder.toString();
	}

	public String getPrimaryLatitudeDMS() {
		return primaryLatitudeDMS;
	}

	public String getPrimaryLongitudeDMS() {
		return primaryLongitudeDMS;
	}

	// Total seconds = 60*60*degrees + 60*minutes + seconds
	public static long getSeconds(String latitudeOrLongitude) {
		long seconds = 0;
		if (latitudeOrLongitude.length() == 8) {// longitude
			long degrees = Long.parseLong(latitudeOrLongitude.substring(0, 3));
			seconds += degrees * 60 * 60;
			long minutes = Long.parseLong(latitudeOrLongitude.substring(3, 5));
			seconds += minutes * 60;
			long secondsFromString = Long.parseLong(latitudeOrLongitude.substring(5, 7));
			seconds += secondsFromString;
		} else {// latitude
			long degrees = Long.parseLong(latitudeOrLongitude.substring(0, 2));
			seconds += degrees * 60 * 60;
			long minutes = Long.parseLong(latitudeOrLongitude.substring(2, 4));
			seconds += minutes * 60;
			long secondsFromString = Long.parseLong(latitudeOrLongitude.substring(4, 6));
			seconds += secondsFromString;
		}

		if (latitudeOrLongitude.endsWith("W") || latitudeOrLongitude.endsWith("S")) {
			return seconds * -1;
		} else {
			return seconds;
		}
	}

	public long getLatitudeInSeconds() {
		return getSeconds(this.primaryLatitudeDMS);
	}

	public long getLongitudeInSeconds() {
		return getSeconds(this.primaryLongitudeDMS);
	}

	/*
	 * Command 6: what_is -l Trimble Knob VA
	 * 
	 * Found matching record at offset 7562:
	 * 
	 * Feature ID : 1496326 Feature Name : Trimble Knob Feature Cat : Summit State : VA County : Highland Latitude :
	 * 382417N Longitude : 793517W Elev in ft : 3123 USGS Quad : Monterey Date created : 09/28/1979
	 * -------------------------------------------------------------------------------- ; ; Let's check the buffer pool:
	 * Command 7: debug pool
	 * 
	 * MRU 7562: 1496326|Trimble
	 * Knob|Summit|VA|51|Highland|091|382417N|0793517W|38.404843|-79.5881037|||||952|3123|Monterey|09/28/1979| LRU
	 */

	public String nonEmptyFieldToString() {
		StringBuilder builder = new StringBuilder();
		if (this.featureID != -1) {
			builder.append("Feature ID\t: " + this.featureID + "\n");
		}
		if (this.featureName != null && !this.featureName.equals("")) {
			builder.append("Feature Name\t: " + this.featureName + "\n");
		}
		if (this.featureClass != null && !this.featureClass.equals("")) {
			builder.append("Feature Cat\t: " + this.featureClass + "\n");
		}
		if (this.stateAlphabeticCode != null && !this.stateAlphabeticCode.equals("")) {
			builder.append("State\t: " + this.stateAlphabeticCode + "\n");
		}
		if (this.countyName != null && !this.countyName.equals("")) {
			builder.append("County\t: " + this.countyName + "\n");
		}
		if (this.primaryLatitudeDMS != null && !this.primaryLatitudeDMS.equals("")) {
			builder.append("Latitude\t: " + this.primaryLatitudeDMS + "\n");
		}
		if (this.primaryLongitudeDMS != null && !this.primaryLongitudeDMS.equals("")) {
			builder.append("Longitude\t: " + this.primaryLongitudeDMS + "\n");
		}
		if (this.sourceLongitudeDMS != null && !this.sourceLongitudeDMS.equals("")) {
			builder.append("Src Long\t: " + this.sourceLongitudeDMS + "\n");
		}
		if (this.sourceLatitudeDMS != null && !this.sourceLatitudeDMS.equals("")) {
			builder.append("Src Lat\t: " + this.sourceLatitudeDMS + "\n");
		}
		if (this.featureElevationFeet != -1) {
			builder.append("Elev in ft\t: " + this.featureElevationFeet + "\n");
		}
		if (this.mapName != null && !this.mapName.equals("")) {
			builder.append("USGS Quad\t: " + this.mapName + "\n");
		}
		if (this.dateCreated != null && !this.dateCreated.equals("")) {
			builder.append("Date created\t: " + this.dateCreated + "\n");
		}
		if (this.dateEdited != null && !this.dateEdited.equals("")) {
			builder.append("Date mod\t: " + this.dateEdited + "\n");
		}
		return builder.toString();
	}

	/**
	 * 
	 * @return pretty-formatted latitude data eg 10d 8m 10s DIRECTION
	 */
	private String prettyPrimaryLatitudeDMS() {
		// convert to integers to strip leading 0
		int dInt = Integer.parseInt(this.primaryLatitudeDMS.substring(0, 2));
		int mInt = Integer.parseInt(this.primaryLatitudeDMS.substring(2, 4));
		int sInt = Integer.parseInt(this.primaryLatitudeDMS.substring(4, 6));
		return dInt
				+ "d "
				+ mInt
				+ "m "
				+ sInt
				+ "s "
				+ this.convertSymbolToCardinalDirection(this.primaryLatitudeDMS.substring(this.primaryLatitudeDMS
						.length() - 1));
	}

	/**
	 * @return pretty-formatted longitude data eg 10d 8m 10s DIRECTION
	 */
	private String prettyPrimaryLongitudeDMS() {
		// convert to integers to strip leading 0
		int dInt = Integer.parseInt(this.primaryLongitudeDMS.substring(0, 3));
		int mInt = Integer.parseInt(this.primaryLongitudeDMS.substring(3, 5));
		int sInt = Integer.parseInt(this.primaryLongitudeDMS.substring(5, 7));
		return dInt
				+ "d "
				+ mInt
				+ "m "
				+ sInt
				+ "s "
				+ this.convertSymbolToCardinalDirection(this.primaryLongitudeDMS.substring(this.primaryLongitudeDMS
						.length() - 1));
	}

	/**
	 * Utility function to pretty-print cardinal directions
	 * 
	 * @param symbol
	 *            {N,S,E,W}
	 * @return {North, South, East, West}
	 */
	private String convertSymbolToCardinalDirection(String symbol) {
		if (symbol.equals("N")) {
			return "North";
		} else if (symbol.equals("W")) {
			return "West";
		} else if (symbol.equals("S")) {
			return "South";
		} else if (symbol.equals("E")) {
			return "East";
		}
		return "";
	}

	public String getFeatureName() {
		return featureName;
	}

	public String getStateAlphabeticCode() {
		return stateAlphabeticCode;
	}

	public String gisFileRepresentation() {
		StringBuilder builder = new StringBuilder();
		if (this.featureID != -1) {
			builder.append(this.featureID);
		}
		builder.append("|");
		if (this.featureName != null && !this.featureName.equals("")) {
			builder.append(this.featureName);
		}
		builder.append("|");
		if (this.featureClass != null && !this.featureClass.equals("")) {
			builder.append(this.featureClass);
		}
		builder.append("|");
		if (this.stateAlphabeticCode != null && !this.stateAlphabeticCode.equals("")) {
			builder.append(this.stateAlphabeticCode);
		}
		builder.append("|");
		if (this.stateNumericCode != -1) {
			builder.append(this.stateNumericCode);
		}
		builder.append("|");
		if (this.countyName != null && !this.countyName.equals("")) {
			builder.append(this.countyName);
		}
		builder.append("|");
		if (this.countyNumericCode != -1) {
			builder.append(this.countyNumericCode);
		}
		builder.append("|");
		if (this.primaryLatitudeDMS != null && !this.primaryLatitudeDMS.equals("")) {
			builder.append(this.primaryLatitudeDMS);
		}
		builder.append("|");
		if (this.primaryLongitudeDMS != null && !this.primaryLongitudeDMS.equals("")) {
			builder.append(this.primaryLongitudeDMS);
		}
		builder.append("|");
		if (this.primaryLatitudeDecDeg != 0.0) {
			builder.append(this.primaryLatitudeDecDeg);
		}
		builder.append("|");
		if (this.primaryLongitudeDecDeg != 0.0) {
			builder.append(this.primaryLongitudeDecDeg);
		}
		builder.append("|");
		if (this.sourceLatitudeDMS != null && !this.sourceLatitudeDMS.equals("")) {
			builder.append(this.sourceLatitudeDMS);
		}
		builder.append("|");
		if (this.sourceLongitudeDMS != null && !this.sourceLongitudeDMS.equals("")) {
			builder.append(this.sourceLongitudeDMS);
		}
		builder.append("|");
		if (this.sourceLatitudeDecDeg != 0.0) {
			builder.append(this.sourceLatitudeDecDeg);
		}
		builder.append("|");
		if (this.sourceLongitudeDecDeg != 0.0) {
			builder.append(this.sourceLongitudeDecDeg);
		}
		builder.append("|");
		if (this.featureElevationMeters != -1) {
			builder.append(this.featureElevationMeters);
		}
		builder.append("|");
		if (this.featureElevationFeet != -1) {
			builder.append(this.featureElevationFeet);
		}
		builder.append("|");
		if (this.mapName != null && !this.mapName.equals("")) {
			builder.append(this.mapName);
		}
		builder.append("|");
		if (this.dateCreated != null && !this.dateCreated.equals("")) {
			builder.append(this.dateCreated);
		}
		builder.append("|");
		if (this.dateEdited != null && !this.dateEdited.equals("")) {
			builder.append(this.dateEdited);
		}
		return builder.toString();
	}

	public String toStringTypeLatLong() {
		return this.countyName + "\t" + this.primaryLongitudeDMS + "\t" + this.primaryLatitudeDMS;
	}

	public String toStringFnameCnameStateAbbrev() {
		return this.featureName + "\t" + this.countyName + "\t" + this.stateAlphabeticCode;
	}

}
