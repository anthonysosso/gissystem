; Script 1
;
; Very basic testing of the simple search commands with a tiny DB.
;
; Specify boundaries of coordinate space:
world	0794130W	0792630W	381500N	383000N

GIS Program

dbFile:     db01.txt
script:     Script01.txt
log:        Log01.txt
Start time: Tue Oct 23 14:53:11 EDT 2012
Quadtree children are printed in the order SW  SE  NE  NW
--------------------------------------------------------------------------------

Latitude/longitude values in index entries are shown as signed integers, in total seconds.

World boundaries are set to:
              138600
   -286890                -285990
              137700
--------------------------------------------------------------------------------
;
; Import some data:
Command 1:  import	VA_Monterey.txt

Imported Features by name: 63
Longest probe sequence:    1
Imported Locations:        63
--------------------------------------------------------------------------------
;
; Check the location and name/state indices:
Command 2:  debug	quad

      *
      [(-286627, 137860), 2346] [(-286628, 137896), 7435] 
      @
      [(-286479, 137945), 2612] [(-286502, 138023), 3037] [(-286523, 138047), 7309] [(-286562, 138080), 8654] 
      *
         [(-286433, 137756), 953] [(-286358, 137773), 1201] [(-286376, 137772), 6560] [(-286356, 137772), 8081] 
         [(-286314, 137725), 2877] [(-286224, 137708), 3720] 
            @
            [(-286304, 137827), 1361] [(-286311, 137845), 4128] 
            *
                  @
            [(-286253, 137904), 1513] [(-286219, 137891), 5669] [(-286229, 137889), 7818] 
            *
         *
      [(-286212, 137722), 4975] [(-286205, 137731), 5259] 
      @
      *
      [(-286438, 137942), 675] [(-286269, 138108), 3593] [(-286231, 138105), 4530] [(-286374, 138012), 6434] 
@
         *
         [(-286273, 138193), 6693] 
            @
         *
            [(-286416, 138293), 7069] 
            [(-286345, 138312), 4696] [(-286337, 138287), 8513] 
                  @
            [(-286353, 138352), 7188] 
            [(-286392, 138367), 265] [(-286393, 138370), 5991] 
      [(-286213, 138302), 4409] 
      @
      *
         [(-286329, 138414), 4819] 
            [(-286301, 138424), 5419] [(-286324, 138413), 5829] 
            *
                  @
            [(-286269, 138486), 4256] 
            [(-286319, 138445), 3438] [(-286321, 138438), 6142] 
            @
            [(-286299, 138530), 2100] [(-286316, 138492), 6940] 
            [(-286231, 138536), 1822, 5102] 
                  @
            [(-286263, 138580), 2223] 
            [(-286313, 138576), 1670] 
            *
            *
                  @
            [(-286379, 138600), 387] [(-286342, 138593), 515] [(-286358, 138591), 3162] [(-286383, 138597), 7942] 
            [(-286403, 138593), 1078] [(-286390, 138593), 1946] [(-286428, 138574), 6811] 
      *
         [(-286561, 138234), 6294] 
         [(-286517, 138257), 7562] 
            @
            *
                  [(-286484, 138266), 2473] [(-286486, 138267), 8225] 
                  *
                              @
                  *
                  [(-286490, 138284), 7682] [(-286486, 138282), 8369] [(-286491, 138282), 8792] 
               *
                        @
               [(-286449, 138309), 797] 
               *
                  @
            *
            [(-286500, 138341), 3879] 
         [(-286638, 138326), 2754] [(-286582, 138315), 4001] 
      @
      [(-286612, 138532), 3314] [(-286475, 138508), 5548] 
      *
--------------------------------------------------------------------------------
Command 3:  debug	hash

Format of display is
Slot number: data record
Current table size is 1019
Number of elements in table is 63

4:  [Asbury Church:VA, [265]]
18:  [Seldom Seen Hollow:VA, [4530]]
21:  [Meadow Draft:VA, [3720]]
37:  [Thorny Bottom Church:VA, [5419]]
44:  [Doe Hill:VA, [6693]]
65:  [New Salem Church:VA, [4128]]
88:  [Smith Field:VA, [7818]]
93:  [Monterey Mountain:VA, [4001]]
127:  [Blue Grass School (historical):VA, [7942]]
131:  [Mount Carlyle:VA, [953]]
132:  [Monterey District:VA, [8654]]
139:  [Forks of Waters:VA, [1822]]
152:  [White Run:VA, [5991]]
170:  [Miracle Ridge:VA, [3879]]
199:  [Hightown Church:VA, [2754]]
212:  [Strait Creek:VA, [5102]]
227:  [Swope Hollow:VA, [5259]]
228:  [Hannah Field Airport:VA, [6294]]
237:  [Lantz Mountain:VA, [3314]]
253:  [Ginseng Mountain:VA, [2100]]
254:  [Town of Monterey:VA, [8792]]
256:  [Monterey:VA, [7682]]
275:  [Central Church:VA, [1078]]
276:  [West Strait Creek:VA, [5829]]
297:  [Simmons Run:VA, [4819]]
323:  [Crab Run:VA, [1361]]
325:  [Highland High School:VA, [2473]]
327:  [Trimble:VA, [7435]]
333:  [Clover Creek Presbyterian Church:VA, [8081]]
334:  [Seybert Hills:VA, [7188]]
365:  [Jack Mountain:VA, [3037]]
391:  [Sounding Knob:VA, [7309]]
406:  [Union Chapel:VA, [5548]]
418:  [Burners Run:VA, [797]]
448:  [Clover Creek:VA, [6560]]
477:  [Peck Run:VA, [4256]]
492:  [Laurel Run:VA, [3438]]
496:  [Gulf Mountain:VA, [2223]]
500:  [New Hampden:VA, [6811]]
546:  [Highland Elementary School:VA, [8225]]
574:  [Possum Trot:VA, [6940]]
590:  [Highland Wildlife Management Area:VA, [2612]]
614:  [Hupman Valley:VA, [2877]]
615:  [Wooden Run:VA, [6142]]
618:  [Trimble Knob:VA, [7562]]
652:  [Rich Hills:VA, [7069]]
663:  [Vance Hollow:VA, [5669]]
670:  [Monterey Methodist Episcopal Church:VA, [8369]]
672:  [Seybert Chapel:VA, [4696]]
677:  [Key Run:VA, [3162]]
734:  [Little Doe Hill:VA, [3593]]
775:  [Blue Grass:VA, [387]]
779:  [Buck Hill:VA, [675]]
813:  [Frank Run:VA, [1946]]
856:  [Elk Run:VA, [1670]]
864:  [Hamilton Chapel:VA, [2346]]
885:  [Bluegrass Valley:VA, [515]]
931:  [Southall Chapel:VA, [4975]]
932:  [Barren Rock:VA, [4409]]
956:  [Strait Creek School (historical):VA, [8513]]
981:  [Davis Run:VA, [1513]]
989:  [Claylick Hollow:VA, [1201]]
998:  [Bear Mountain:VA, [6434]]
--------------------------------------------------------------------------------
;
; Let's try some variations of a single-match name/state search:
Command 4:  what_is	-c	Trimble Knob	VA

The number of records for Trimble Knob and VA was 1
--------------------------------------------------------------------------------
Command 5:  what_is	Trimble Knob	VA

7562:  Highland  793517W  382417N
--------------------------------------------------------------------------------
Command 6:  what_is	-l	Trimble Knob	VA

Found matching record at offset 7562:

  Feature ID   : 1496326
  Feature Name : Trimble Knob
  Feature Cat  : Summit
  State        : VA
  County       : Highland
  Latitude     : 382417N
  Longitude    : 793517W
  Elev in ft   : 3123
  USGS Quad    : Monterey
  Date created : 09/28/1979
--------------------------------------------------------------------------------
;
; Let's check the buffer pool:
Command 7:  debug	pool

MRU
 7562:  1496326|Trimble Knob|Summit|VA|51|Highland|091|382417N|0793517W|38.404843|-79.5881037|||||952|3123|Monterey|09/28/1979|
LRU
--------------------------------------------------------------------------------
;
; Let's try some variations of a single-match location search:
Command 8:  what_is_at	-c	382812N	0793156W

The number of records for 793156W and 382812N was 1
--------------------------------------------------------------------------------
Command 9:  what_is_at	382812N	0793156W

   The following features were found at 793156W   382812N:
6940:  Possum Trot  Highland  VA
--------------------------------------------------------------------------------
Command 10:  what_is_at	-l	382812N	0793156W

   The following features were found at 793156W   382812N:
  Feature ID   : 1496110
  Feature Name : Possum Trot
  Feature Cat  : Populated Place
  State        : VA
  County       : Highland
  Latitude     : 382812N
  Longitude    : 793156W
  Elev in ft   : 2520
  USGS Quad    : Monterey
  Date created : 09/28/1979

--------------------------------------------------------------------------------
;
; Let's check the buffer pool:
Command 11:  debug	pool

MRU
 6940:  1496110|Possum Trot|Populated Place|VA|51|Highland|091|382812N|0793156W|38.4701196|-79.5322693|||||768|2520|Monterey|09/28/1979|
 7562:  1496326|Trimble Knob|Summit|VA|51|Highland|091|382417N|0793517W|38.404843|-79.5881037|||||952|3123|Monterey|09/28/1979|
LRU
--------------------------------------------------------------------------------
;
; Let's try a few more name/state searches:
Command 12:  what_is	-l	Swope Hollow	VA

Found matching record at offset 5259:

  Feature ID   : 1487758
  Feature Name : Swope Hollow
  Feature Cat  : Valley
  State        : VA
  County       : Highland
  Latitude     : 381531N
  Longitude    : 793005W
  Src Long     : 381619N
  Src Lat      : 793014W
  Elev in ft   : 1775
  USGS Quad    : Monterey SE
  Date created : 09/28/1979
--------------------------------------------------------------------------------
Command 13:  what_is	-l	Possum Trot	VA

Found matching record at offset 6940:

  Feature ID   : 1496110
  Feature Name : Possum Trot
  Feature Cat  : Populated Place
  State        : VA
  County       : Highland
  Latitude     : 382812N
  Longitude    : 793156W
  Elev in ft   : 2520
  USGS Quad    : Monterey
  Date created : 09/28/1979
--------------------------------------------------------------------------------
Command 14:  what_is	-l	Blue Grass	VA

Found matching record at offset 387:

  Feature ID   : 1481852
  Feature Name : Blue Grass
  Feature Cat  : Populated Place
  State        : VA
  County       : Highland
  Latitude     : 383000N
  Longitude    : 793259W
  Elev in ft   : 2549
  USGS Quad    : Monterey
  Date created : 09/28/1979
--------------------------------------------------------------------------------
;
; Let's check the buffer pool:
Command 15:  debug	pool

MRU
  387:  1481852|Blue Grass|Populated Place|VA|51|Highland|091|383000N|0793259W|38.5001188|-79.5497702|||||777|2549|Monterey|09/28/1979|
 6940:  1496110|Possum Trot|Populated Place|VA|51|Highland|091|382812N|0793156W|38.4701196|-79.5322693|||||768|2520|Monterey|09/28/1979|
 5259:  1487758|Swope Hollow|Valley|VA|51|Highland|091|381531N|0793005W|38.2587359|-79.5014329|381619N|0793014W|38.2719444|-79.5038889|541|1775|Monterey SE|09/28/1979|
 7562:  1496326|Trimble Knob|Summit|VA|51|Highland|091|382417N|0793517W|38.404843|-79.5881037|||||952|3123|Monterey|09/28/1979|
LRU
--------------------------------------------------------------------------------
;
; Let's try a few more location searches:
Command 16:  what_is_at	-l	382145N	0793031W

   The following features were found at 793031W   382145N:
  Feature ID   : 1486995
  Feature Name : Seldom Seen Hollow
  Feature Cat  : Valley
  State        : VA
  County       : Highland
  Latitude     : 382145N
  Longitude    : 793031W
  Src Long     : 382227N
  Src Lat      : 793004W
  Elev in ft   : 2461
  USGS Quad    : Monterey SE
  Date created : 09/28/1979

--------------------------------------------------------------------------------
Command 17:  what_is_at	-l	382442N	0793451W

   The following features were found at 793451W   382442N:
  Feature ID   : 2391311
  Feature Name : Town of Monterey
  Feature Cat  : Civil
  State        : VA
  County       : Highland
  Latitude     : 382442N
  Longitude    : 793451W
  Elev in ft   : 2900
  USGS Quad    : Monterey
  Date created : 02/19/2008

--------------------------------------------------------------------------------
Command 18:  what_is_at	-l	382607N	0793312W

   The following features were found at 793312W   382607N:
  Feature ID   : 1481345
  Feature Name : Asbury Church
  Feature Cat  : Church
  State        : VA
  County       : Highland
  Latitude     : 382607N
  Longitude    : 793312W
  Elev in ft   : 2684
  USGS Quad    : Monterey
  Date created : 09/28/1979

--------------------------------------------------------------------------------
;
; Let's check the buffer pool:
Command 19:  debug	pool

MRU
  265:  1481345|Asbury Church|Church|VA|51|Highland|091|382607N|0793312W|38.4353981|-79.5533807|||||818|2684|Monterey|09/28/1979|
 8792:  2391311|Town of Monterey|Civil|VA|51|Highland|091|382442N|0793451W|38.4115829|-79.580854|||||884|2900|Monterey|02/19/2008|
 4530:  1486995|Seldom Seen Hollow|Valley|VA|51|Highland|091|382145N|0793031W|38.3626223|-79.5086563|382227N|0793004W|38.3741667|-79.5011111|750|2461|Monterey SE|09/28/1979|
  387:  1481852|Blue Grass|Populated Place|VA|51|Highland|091|383000N|0793259W|38.5001188|-79.5497702|||||777|2549|Monterey|09/28/1979|
 6940:  1496110|Possum Trot|Populated Place|VA|51|Highland|091|382812N|0793156W|38.4701196|-79.5322693|||||768|2520|Monterey|09/28/1979|
 5259:  1487758|Swope Hollow|Valley|VA|51|Highland|091|381531N|0793005W|38.2587359|-79.5014329|381619N|0793014W|38.2719444|-79.5038889|541|1775|Monterey SE|09/28/1979|
 7562:  1496326|Trimble Knob|Summit|VA|51|Highland|091|382417N|0793517W|38.404843|-79.5881037|||||952|3123|Monterey|09/28/1979|
LRU
--------------------------------------------------------------------------------
;
; Let's try a two-match location search:
Command 20:  what_is_at	382856N	0793031W

   The following features were found at 793031W   382856N:
1822:  Forks of Waters  Highland  VA
5102:  Strait Creek  Highland  VA
--------------------------------------------------------------------------------
;
; Let's check the buffer pool:
Command 21:  debug	pool

MRU
 5102:  1487661|Strait Creek|Stream|VA|51|Highland|091|382856N|0793031W|38.4823417|-79.5086575|382442N|0793222W|38.4116667|-79.5394444|705|2313|Monterey|09/28/1979|
 1822:  1483492|Forks of Waters|Locale|VA|51|Highland|091|382856N|0793031W|38.4823417|-79.5086575|||||705|2313|Monterey|09/28/1979|
  265:  1481345|Asbury Church|Church|VA|51|Highland|091|382607N|0793312W|38.4353981|-79.5533807|||||818|2684|Monterey|09/28/1979|
 8792:  2391311|Town of Monterey|Civil|VA|51|Highland|091|382442N|0793451W|38.4115829|-79.580854|||||884|2900|Monterey|02/19/2008|
 4530:  1486995|Seldom Seen Hollow|Valley|VA|51|Highland|091|382145N|0793031W|38.3626223|-79.5086563|382227N|0793004W|38.3741667|-79.5011111|750|2461|Monterey SE|09/28/1979|
  387:  1481852|Blue Grass|Populated Place|VA|51|Highland|091|383000N|0793259W|38.5001188|-79.5497702|||||777|2549|Monterey|09/28/1979|
 6940:  1496110|Possum Trot|Populated Place|VA|51|Highland|091|382812N|0793156W|38.4701196|-79.5322693|||||768|2520|Monterey|09/28/1979|
 5259:  1487758|Swope Hollow|Valley|VA|51|Highland|091|381531N|0793005W|38.2587359|-79.5014329|381619N|0793014W|38.2719444|-79.5038889|541|1775|Monterey SE|09/28/1979|
 7562:  1496326|Trimble Knob|Summit|VA|51|Highland|091|382417N|0793517W|38.404843|-79.5881037|||||952|3123|Monterey|09/28/1979|
LRU
--------------------------------------------------------------------------------
;
; Exit
Command 22:  quit	

Terminating execution of commands.
End time: Tue Oct 23 14:53:12 EDT 2012
--------------------------------------------------------------------------------
